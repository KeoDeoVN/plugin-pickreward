package kdvn.sky2.pickreward.main;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;

import kdvn.sky2.pickreward.object.PRInvSet;
import kdvn.sky2.pickreward.object.PRReward;

public class PRListener implements Listener {
	
	@EventHandler
	public void onClick(InventoryClickEvent e) {
		Inventory inv = e.getInventory();
		if (!(inv.getHolder() instanceof PRReward)) return;
		e.setCancelled(true);
		
		Player player = (Player) e.getWhoClicked();
		if (inv.getHolder() instanceof PRReward) {
			e.setCancelled(true);
			PRReward reward = (PRReward) inv.getHolder();
			int slot = e.getSlot();
			if (!PRUtils.pick(player, reward, slot, inv)) {
				return;
			} else player.sendMessage("§aMở quà thành công");
		}
	}
	
	@EventHandler
	public void onClose(InventoryCloseEvent e) {
		Inventory inv = e.getInventory();
		Player player = (Player) e.getPlayer();
		if (inv.getHolder() instanceof PRReward) {
			PRReward reward = (PRReward) inv.getHolder();
			PRUtils.closeReward(player, reward);
			player.sendMessage("§aĐóng bảng chọn quà");
		}
	}
	
	@EventHandler
	public void onClose2(InventoryCloseEvent e) {
		Inventory inv = e.getInventory();
		Player player = (Player) e.getPlayer();
		if (e.getView().getTitle().contains(PRInvSet.TITLE)) {
			String id = e.getView().getTitle().replace(PRInvSet.TITLE, "");
			PRInvSet.save(id, inv);
			player.sendMessage("§aSaved");
		}
	}
	
	public static void main(String[] args) {
		int count = 0;
		int n = 2;
		int k = 3;
		int p = 4;
		for (int i = mu(10, n - 1) ; i < mu(10, n) ; i++) {
			if (tongchuso(i) == k) count++;
			if (count == p) {
				System.out.println("Số thứ " + p + " là " + i);
			}
		}
	}
	
	public static int mu(int a, int b) {
		int r = 1;
		for (int i = 0 ; i < b ; i++) {
			r *= a;
		}
		return r;
	}
	
	public static int tongchuso(int a) {
		int r = 0;
		while (a > 0) {
			r = r + a % 10;
			a = a / 10;
		}
		return r;
	}
	
}
