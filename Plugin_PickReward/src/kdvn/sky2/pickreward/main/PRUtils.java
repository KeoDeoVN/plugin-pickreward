package kdvn.sky2.pickreward.main;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import kdvn.sky2.pickreward.object.PRData;
import kdvn.sky2.pickreward.object.PRReward;

public class PRUtils {
	
	private static Map<String, PRReward> rewards = new HashMap<String, PRReward> ();
	private static Map<String, PRData> pick = new HashMap<String, PRData> ();
	
	public static Optional<PRReward> get(String id) {
		PRReward reward = null;
		if (rewards.containsKey(id)) reward = rewards.get(id);
		return Optional.of(reward);
	}
	
	public static Map<String, PRReward> getAll() {
		return rewards;
	}
	
	public static void remove(String id) {
		if (rewards.containsKey(id)) rewards.remove(id);
	}
	
	public static void clear() {
		rewards.clear();
	}
	
	public static void add(String id, PRReward reward) {
		rewards.put(id, reward);
	}
	
	public static void openReward(Player player, String id) {
		Optional<PRReward> oreward = get(id);
		if (!oreward.isPresent()) {
			player.sendMessage("§cXảy ra lỗi, hãy báo với admin");
			return;
		}
		PRReward reward = oreward.get();
		openReward(player, reward);
	}
	
	public static void openReward(Player player, PRReward reward) {
		Inventory inv = getInv(reward);
		player.openInventory(inv);
		PRData data = new PRData(shuffleItems(reward.getItemStacks()), reward);
		pick.put(player.getName(), data);
		
		Bukkit.getScheduler().runTaskLaterAsynchronously(MainSky2PR.getMain(), () -> {
			for (int i = 0 ; i < inv.getSize() ; i++) {
				inv.setItem(i, data.getItemStacks()[i]);
				player.playSound(player.getLocation(), Sound.BLOCK_WOOD_BUTTON_CLICK_OFF, 1, 1);
			}
		}, MainSky2PR.OPEN_SECONDS * 20);
	}
	
	public static void closeReward(Player player, PRReward reward) {
		if (pick.containsKey(player.getName())) pick.remove(player.getName());
	}
	
	public static List<Integer> shuffleSlots(int size) {
		List<Integer> original = new ArrayList<Integer> ();
		for (int i = 0 ; i < size ; i++) original.add(i);
		Collections.shuffle(original);
		return original;
 	}
	
	public static ItemStack[] shuffleItems(ItemStack[] items) {
		ItemStack[] clone = new ItemStack[items.length];
		List<Integer> list = shuffleSlots(items.length);
		for (int i = 0 ; i < list.size() ; i++) {
			clone[i] = items[list.get(i)];
		}
		return clone;
	}
	
	public static Inventory getInv(String id) {
		Optional<PRReward> oreward = get(id);
		if (!oreward.isPresent()) {
			System.out.println("§c§lERROR: GET REWARD NULL");
			return null;
		}
		PRReward reward = oreward.get();
		return getInv(reward);
	}
	
	public static Inventory getInv(PRReward reward) {
		Inventory inv = Bukkit.createInventory(reward, reward.getItemStacks().length, reward.getTitle());
		for (int i = 0 ; i < reward.getItemStacks().length ; i++) {
			inv.setItem(i, getChestSlot());
		}
		return inv;
	}
	
	public static ItemStack getChestSlot() {
		ItemStack item = new ItemStack(Material.CHEST);
		ItemStackUtils.setDisplayName(item, "§a§lChọn phần thưởng");
		ItemStackUtils.addLoreLine(item, "§f> Click để chọn");
		
		return item;
	}
	
	public static boolean pick(Player player, PRReward reward, int slot, Inventory inv) {
		if (!pick.containsKey(player.getName())) return false;
		PRData data = pick.get(player.getName());
		if (data.getPickTime() >= getPickTime(player)) {
			player.sendMessage("§cMua §eVIP §cđể tăng số lần mở");
			return false;
		}
		
		ItemStack clicked = inv.getItem(slot);
		if (!clicked.isSimilar(getChestSlot())) {
			return false;
		}
		
		ItemStack result = data.getItemStacks()[slot];
		data.setPickTime(data.getPickTime() + 1);
		player.getInventory().addItem(result);

		inv.setItem(slot, result);
		
		player.playSound(player.getLocation(), Sound.BLOCK_WOOD_BUTTON_CLICK_ON, 1, 1);
		
		return true;
	}
	
	public static Inventory clone(Inventory inv, int invSize) {
		Inventory cinv = Bukkit.createInventory(inv.getHolder(), invSize, "");
		for (int i = 0 ; i < invSize ; i++) {
			cinv.setItem(i, inv.getItem(i));
		}
		return cinv;
	}
	
    public static Inventory fromBase64(String data, String title) throws IOException {
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
            
            int size =  dataInput.readInt();
            Inventory inventory = Bukkit.getServer().createInventory(null, size, title);
            
            // Read the serialized inventory
            for (int i = 0; i < inventory.getSize(); i++) {
                inventory.setItem(i, (ItemStack) dataInput.readObject());
            }
            dataInput.close();
            inputStream.close();
            return inventory;
        } catch (ClassNotFoundException e) {
            throw new IOException("Unable to decode class type.", e);
        }
    }
    
    public static Map<Integer, String> toBase64ItemStackm(Map<Integer, ItemStack> items) {
        try { 
            Map<Integer, String> map = new HashMap<Integer, String> ();
            
            for (int i : items.keySet()) {
            	map.put(i, toBase64ItemStack(items.get(i)));
            }
            return map;
        } catch (Exception e) {
            throw new IllegalStateException("Unable to save item stacks.", e);
        }        
    }
    
    public static ItemStack toItemStack(String base64) {
    	try {
        	ByteArrayInputStream bais = new ByteArrayInputStream(Base64Coder.decodeLines(base64));
			BukkitObjectInputStream ois = new BukkitObjectInputStream(bais);
			
			ItemStack item = (ItemStack) ois.readObject();
			ois.close();
			bais.close();
			return item;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return null;
    }
	
    public static String toBase64(Inventory inventory) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);
            
            // Write the size of the inventory
            dataOutput.writeInt(inventory.getSize());
            
            // Save every element in the list
            for (int i = 0; i < inventory.getSize(); i++) {
                dataOutput.writeObject(inventory.getItem(i));
            }
            
            // Serialize that array
            dataOutput.close();
            outputStream.close();
            return Base64Coder.encodeLines(outputStream.toByteArray());
        } catch (Exception e) {
            throw new IllegalStateException("Unable to save item stacks.", e);
        }        
    }
    
    public static String toBase64ItemStack(ItemStack item) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);
            
            dataOutput.writeObject(item);
            
            // Serialize that array
            dataOutput.close();
            outputStream.close();
            return Base64Coder.encodeLines(outputStream.toByteArray());
        } catch (Exception e) {
            throw new IllegalStateException("Unable to save item stacks.", e);
        }        
    }
	
    public static int getPickTime(Player player) {
    	int pickTime = 1;
    	for (int i = 2 ; i <= 10 ; i++) {
    		if (player.hasPermission("pickreward." + i)) {
    			pickTime = i;
    		}
    	}
    	return pickTime;
    }
	
	
}
