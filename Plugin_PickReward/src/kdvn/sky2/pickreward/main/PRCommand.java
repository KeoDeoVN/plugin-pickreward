package kdvn.sky2.pickreward.main;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import kdvn.sky2.pickreward.object.PRInvSet;
import kdvn.sky2.pickreward.object.PRReward;

public class PRCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		
		if (!sender.hasPermission("pr.*")) return false;
		try {
			
			if (args[0].equalsIgnoreCase("create")) {
				Player player = (Player) sender;
				String id = args[1];
				String name = args[2].replace("_", " ");
				int invSize = Integer.parseInt(args[3]);
				
				PRReward reward = new PRReward(name, null, invSize);
				PRUtils.add(id, reward);
				PRInvSet.openGUI(player, id);
				
				sender.sendMessage("Ok");
			}
			
			else if (args[0].equalsIgnoreCase("setinv")) {
				String id = args[1];
				Player player = (Player) sender;
				PRInvSet.openGUI(player, id);
			}
			
			else if (args[0].equalsIgnoreCase("remove")) {
				String id = args[1];
				PRUtils.remove(id);
				MainSky2PR.getMain().saveConfig();
				
				sender.sendMessage("Ok");
			}
			
			else if (args[0].equalsIgnoreCase("reload")) {
				MainSky2PR.getMain().reloadConfig();
				sender.sendMessage("Ok");
			}
			
			else if (args[0].equalsIgnoreCase("open")) {
				Player player = Bukkit.getPlayer(args[2]);
				String id = args[1];
				PRUtils.openReward(player, id);
			}
			
		}
		catch (ArrayIndexOutOfBoundsException e) {
			sendTut(sender);
		}
		
		
		return false;
	}
	
	public void sendTut(CommandSender sender) {
		sender.sendMessage("/pr reload: Reload");
		sender.sendMessage("/pr create <id> <name(' ' = '_')> <invSize>: Create");
		sender.sendMessage("/pr setinv <id>: Set");
		sender.sendMessage("/pr remove <id>: Remove");
		sender.sendMessage("/pr open <id> <player>: Open");
	}

}
