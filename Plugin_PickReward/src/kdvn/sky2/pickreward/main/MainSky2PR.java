package kdvn.sky2.pickreward.main;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;

import kdvn.sky2.pickreward.object.PRReward;

public class MainSky2PR extends JavaPlugin {
	
	public static final int OPEN_SECONDS = 10;
	
	private static MainSky2PR main;
	public static MainSky2PR getMain() {return main;}
	
	private FileConfiguration config;
	
	@Override
	public void onEnable() {
		// Data
		main = this;
		this.saveDefaultConfig();
		this.reloadConfig();
		
		//
		this.getCommand("pr").setExecutor(new PRCommand());
		
		// 
		Bukkit.getPluginManager().registerEvents(new PRListener(), this);
	}
	
	public void reloadConfig() {
		File file = new File(this.getDataFolder(), "config.yml");
		config = YamlConfiguration.loadConfiguration(file);
		loadAll();
	}
	
	public void saveConfig() {
		config.set("pickreward", null);
		// Save all
		PRUtils.getAll().forEach((id, reward) -> {
			save(id, reward);
		});
		File file = new File(this.getDataFolder(), "config.yml");
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public FileConfiguration getConfig() {
		return this.config;
	}
	
	public void loadAll() {
		PRUtils.clear();
		if (!config.contains("pickreward")) return;
		config.getConfigurationSection("pickreward").getKeys(false).forEach(id -> {
			try {
				String name = config.getString("pickreward." + id + ".name");
				int invSize = config.getInt("pickreward." + id + ".inv-size");
				Inventory inv =PRUtils.fromBase64(config.getString("pickreward." + id + ".inv"), "");
				PRReward reward = new PRReward(name, inv, invSize);
				PRUtils.add(id, reward);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}
	
	public void save(String id, PRReward reward) {
		config.set("pickreward." + id + ".name", reward.getName());
		config.set("pickreward." + id + ".inv-size", reward.getInventorySize());
		config.set("pickreward." + id + ".inv", PRUtils.toBase64(reward.getInventory()));;
	}
	
}
