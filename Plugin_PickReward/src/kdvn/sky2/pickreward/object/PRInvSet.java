package kdvn.sky2.pickreward.object;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import kdvn.sky2.pickreward.main.MainSky2PR;
import kdvn.sky2.pickreward.main.PRUtils;

public class PRInvSet {

	public static final String TITLE = "§c§lPUT YOUR FUCKING ITEMS HERE ";
	
	public static void openGUI(Player player, String id) {
		PRReward reward = PRUtils.get(id).get();
		int size = reward.getInventorySize();
		Inventory inv = Bukkit.createInventory(null, size, TITLE + id);
		player.openInventory(inv);
		if (reward.getItemStacks() == null) return;
		for (int i = 0 ; i < reward.getInventorySize() ; i++) {
			inv.setItem(i, reward.getItemStacks()[i]);
		}
	}
	
	public static void save(String id, Inventory inv) {
		PRReward reward = PRUtils.get(id).get();
		PRReward r = new PRReward(reward.getName(), inv, inv.getSize());
		PRUtils.add(id, r);
		MainSky2PR.getMain().saveConfig();
	}

}
