package kdvn.sky2.pickreward.object;

import org.bukkit.inventory.ItemStack;

public class PRData {
	
	private int pickTime = 0;
	private ItemStack[] itemStacks;
	private PRReward reward;
	
	public PRData(ItemStack[] itemStacks, PRReward reward) {
		this.itemStacks = itemStacks;
		this.reward = reward;
	}
	
	public int getPickTime() {
		return this.pickTime;
	}
	
	public PRReward getReward() {
		return this.reward;
	}
	
	public ItemStack[] getItemStacks() {
		return this.itemStacks;
	}
	
	public void setPickTime(int value) {
		this.pickTime = value;
	}
	
}
