package kdvn.sky2.pickreward.object;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

public class PRReward implements InventoryHolder {
	
	private String name;
	private Inventory inv;
	private int invSize;
	private ItemStack[] items = null;
	
	public PRReward(String name, Inventory inv, int invSize) {
		this.name = name;
		this.invSize = invSize;
		this.inv = inv;
		if (inv == null) return;
		this.items = new ItemStack[invSize];
		for (int i = 0 ; i < invSize ; i++) {
			this.items[i] = inv.getItem(i);
		}
	}
	
	public String getName() {
		return this.name;
	}
	
	public ItemStack[] getItemStacks() {
		return this.items;
	}
	
	public String getTitle() {
		return "§c§l" + name.toUpperCase() + " §2§lCHỌN QUÀ";
	}

	public int getInventorySize() {
		return this.invSize;
	}
	
	@Override
	public Inventory getInventory() {
		return this.inv;
	}
	
	
}
